import axios from 'axios';
import { $ } from './bling'

const mapOptions = {
    center:{lat:43.2, lng: -79.8},
    zoom: 12
}

function loadPLaces(map, lat = 43.2, lng = -79.8 ) {
    axios.get(`/api/stores/near?lat=${lat}&lng=${lng}`)
        .then(res => {
            const places = res.data;
            if(!places.length){
                return;
            }

            const bounds = new google.maps.LatLngBounds();
            const infoWindow = new google.maps.InfoWindow();

            const markers = places.map(place => {
                const[placeLng, placeLat] = place.location.coordinates;
                const position = { lat:placeLat, lng:placeLng };
                bounds.extend(position);
                const marker = new google.maps.Marker({map, position});
                marker.place = place;
                return marker;
            });

            // when someone clicks on a marker, show the details of that place
            markers.forEach(marker => marker.addListener('click', function() {
                console.log(this.place);
                const html = `
                  <div class="popup">
                    <a href="/store/${this.place.slug}">
                      <img src="/uploads/${this.place.photo || 'store.png'}" alt="${this.place.name}" />
                      <p>${this.place.name} - ${this.place.location.address}</p>
                    </a>
                  </div>
        `;
                infoWindow.setContent(html);
                infoWindow.open(map, this);
            }));


            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        })
}

function makeMapDiv(mapDiv) {
    if(!mapDiv) return;
    const map = new google.maps.Map(mapDiv, mapOptions);
    const input = $("[name=geolocate]");
    const autocomplete = new google.maps.places.Autocomplete(input);
    loadPLaces(map);

    autocomplete.addListener('place_changed', () => {
        const place = autocomplete.getPlace();
        loadPLaces(map, place.geometry.location.lat(), place.geometry.location.lng());
    })
}

export default makeMapDiv;